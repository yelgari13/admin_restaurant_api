import { compareSync, hashSync, genSaltSync } from 'bcryptjs';

/**
 * @param {string} value
 */
export function encrypt(value) {
  const salt = genSaltSync(10);
  return hashSync(value, salt);
}

/**
 * @param {string} entry
 * @param {string} hash
 */
export function compareHashed(entry, hash) {
  return compareSync(entry, hash);
}
