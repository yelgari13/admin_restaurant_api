const regex = {
  regexRuc: /^([0-9]{11})$/,
  regexURL: /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!]))?/,
};

export default regex;
