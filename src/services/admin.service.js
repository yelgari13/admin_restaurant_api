import { v4 as uuidv4 } from 'uuid';
import { encrypt } from '../common/helper';

export default class AdminService {
  constructor({ cloudConnectionMySqlDb }) {
    this._mysqlImpl = cloudConnectionMySqlDb;
  }

  async getByUsername(username) {
    const queryString = 'call sp_admin_by_username(?)';
    const [rows] = await this._mysqlImpl.query(queryString, { values: [username] });
    return rows[0].length ? rows[0][0] : null;
  }

  async create(entry) {
    const id = uuidv4();
    const entity = {
      adminId: id,
      adminName: entry.name,
      adminUsername: entry.username,
      adminPassword: encrypt(entry.password),
    };

    const queryString = 'call sp_insert_admin(?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });

    return { id };
  }
}
