import { v4 as uuidv4 } from 'uuid';

export default class RestaurantService {
  constructor({ cloudConnectionMySqlDb, store }) {
    this._mysqlImpl = cloudConnectionMySqlDb;
    this.store = store;
  }

  async getByAdmin() {
    const { adminId } = this.store.ctx();
    const queryString = 'call sp_restaurant_by_admin(?)';
    const [rows] = await this._mysqlImpl.query(queryString, { values: [adminId] });
    return rows[0].length ? rows[0][0] : null;
  }

  async create(entry) {
    const id = uuidv4();
    const entity = {
      restaurantId: id,
      adminId: entry.adminId,
      restaurantRuc: entry.ruc,
      restaurantName: entry.name,
      restaurantLogoUrl: entry.logoUrl,
      currencyId: entry.currencyId,
      country: entry.country,
      departament: entry.departament,
      province: entry.province,
      district: entry.district,
      address: entry.address,
      addressRefence: entry.addressRefence,
    };

    const queryString = 'call sp_insert_restaurant(?,?,?,?,?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });

    return { id };
  }
}
