import { v4 as uuidv4 } from 'uuid';

export default class ProductService {
  constructor({ cloudConnectionMySqlDb, store }) {
    this._mysqlImpl = cloudConnectionMySqlDb;
    this.store = store;
  }

  async search(restaurantId, filter) {
    const { adminId } = this.store.ctx();
    const queryString = `SELECT * FROM product WHERE adminId = '${adminId}' AND restaurantId = '${restaurantId}' <<conditions>>`;
    const queryBuilder = this._setBuildFilter(filter, queryString);
    const [rows] = await this._mysqlImpl.query(queryBuilder);
    return rows;
  }

  _setBuildFilter(filter, query) {
    const conditions = [];

    if (filter.productName) {
      conditions.push(`productName LIKE '${filter.productName}%'`);
    }

    // Agrega más condiciones según sea necesario para otros filtros

    return query.replace('<<conditions>>', conditions.length ? `AND ${conditions.join(' AND ')}` : '');
  }

  async list() {
    const queryString = 'call sp_list_product()';
    const [rows] = await this._mysqlImpl.query(queryString);
    return rows;
  }

  async getById(productId) {
    const queryString = 'call sp_product_byId(?)';
    const [rows] = await this._mysqlImpl.query(queryString, { values: [productId] });
     return rows[0].length ? rows[0][0] : null;
  }

  async create(entry) {
    const { adminId } = this.store.ctx();
    const Id = uuidv4();
    const entity = {
      productId: Id,
      restaurantId: entry.restaurantId,
      adminId,
      productName: entry.productName,
      productType: entry.productType,
      productBrand: entry.productBrand,
      productDescription: entry.productDescription,
      productStock: entry.productStock,
      productPrice: entry.productPrice,
      productDiscount: entry.productDiscount,
      currencySymbol: entry.currencySymbol,
      isDelete: false,
    };

    const queryString = 'call sp_insert_product(?,?,?,?,?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });

    return { Id };
  }

  async update(productId, entry) {
    const entity = {
      productId,
      productType: entry.productType,
      productBrand: entry.productBrand,
      productDescription: entry.productDescription,
      productStock: entry.productStock,
      productPrice: entry.productPrice,
      productDiscount: entry.productDiscount,
      currencySymbol: entry.currencySymbol,
    };

    const queryString = 'call sp_update_product(?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });
  }

  async delete(Id) {
    const queryString = 'call sp_delete_product(?)';
    await this._mysqlImpl.query(queryString, { values: [Id] });
  }
}
