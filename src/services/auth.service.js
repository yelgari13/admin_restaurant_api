import AppError from '../utils/appError.util';
import { generateToken } from '../utils/jwt.util';
import { compareHashed } from '../common/helper';

export default class AuthService {
  constructor({ adminService }) {
    this._adminService = adminService;
  }

  async login(username, password) {
    const admin = await this._adminService.getByUsername(username);

    if (!admin) {
      throw new AppError(400, 'ADMIN_DOES_NOT_EXIST', `The username ${username} does not exist`);
    }

    const validatePassword = compareHashed(password, admin.adminPassword);

    if (!validatePassword) {
      throw new AppError(400, 'PASSWORD_IS_INVALID', `The password by ${username} is invalid`);
    }

    const encode = this._adminToEncode(admin);

    return { token: generateToken(encode) };
  }

  _adminToEncode(data) {
    return {
      adminId: data.adminId,
      adminName: data.adminName,
      adminUsername: data.adminUsername,
    };
  }
}
