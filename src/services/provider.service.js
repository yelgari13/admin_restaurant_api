import { v4 as uuidv4 } from 'uuid';

export default class ProviderService {
  constructor({ cloudConnectionMySqlDb, store }) {
    this._mysqlImpl = cloudConnectionMySqlDb;
    this.store = store;
  }

  async search(restaurantId, filter) {
    const { adminId } = this.store.ctx();
    const queryString = `select * from provider as p where p.adminId = '${adminId}' and p.restaurantId = '${restaurantId}' <<conditions>>`;
    const queryBuilder = this._setBuildFilter(filter, queryString);
    const [rows] = await this._mysqlImpl.query(queryBuilder);
    return rows;
  }

  _setBuildFilter(filter, query) {
    const conditions = [];

    if (filter.providerBussinessName) {
      conditions.push(`providerBussinessName LIKE '${filter.providerBussinessName}%'`);
    }

    return query.replace('<<conditions>>', conditions.length ? `and ${conditions.join(' and ')}` : '');
  }

  async list() {
    const queryString = 'call sp_list_provider()';
    const [rows] = await this._mysqlImpl.query(queryString);
    return rows[0];
  }

  async getById(id) {
    const queryString = 'call sp_provider_byId(?)';
    const [rows] = await this._mysqlImpl.query(queryString, { values: [id] });
    return rows[0].length ? rows[0][0] : null;
  }

  async create(entry) {
    const { adminId } = this.store.ctx();
    const id = uuidv4();
    const entity = {
      providerId: id,
      restaurantId: entry.restaurantId,
      adminId,
      providerBussinessName: entry.providerBussinessName,
      providerRuc: entry.providerRuc,
      providerPhone: entry.providerPhone,
      providerEmail: entry.providerEmail,
      country: entry.country,
      departament: entry.departament,
      province: entry.province,
      district: entry.district,
      address: entry.address,
      addressReference: entry.addressReference,
      IsDelete: false,
    };

    const queryString = 'call sp_insert_provider(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });

    return { id };
  }

  async update(providerId, entry) {
    const entity = {
      providerId,
      providerPhone: entry.providerPhone,
      providerEmail: entry.providerEmail,
      country: entry.country,
      departament: entry.departament,
      province: entry.province,
      district: entry.district,
      address: entry.address,
      addressReference: entry.addressReference,
    };

    const queryString = 'call sp_update_provider(?,?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });
  }

  async delete(id) {
    const queryString = 'call sp_delete_provider(?)';
    await this._mysqlImpl.query(queryString, { values: [id] });
  }
}
