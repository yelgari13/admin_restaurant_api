import { v4 as uuidv4 } from 'uuid';

export default class EmployeeService {
  constructor({ cloudConnectionMySqlDb, store }) {
    this._mysqlImpl = cloudConnectionMySqlDb;
    this.store = store;
  }

  async search(restaurantId, filter) {
    const { adminId } = this.store.ctx();
    const queryString = `SELECT * FROM employee WHERE adminId = '${adminId}' AND restaurantId = '${restaurantId}' <<conditions>>`;
    const queryBuilder = this._setBuildFilter(filter, queryString);
    const [rows] = await this._mysqlImpl.query(queryBuilder);
    return rows;
  }

  _setBuildFilter(filter, query) {
    const conditions = [];

    if (filter.employeeName) {
      conditions.push(`employeeName LIKE '${filter.employeeName}%'`);
    }

    return query.replace('<<conditions>>', conditions.length ? `AND ${conditions.join(' AND ')}` : '');
  }

  async list() {
    const queryString = 'CALL sp_list_employee()';
    const [rows] = await this._mysqlImpl.query(queryString);
    return rows[0];
  }

  async getById(id) {
    const queryString = `select * from employee where employeeId = '${id}'`;
    const [rows] = await this._mysqlImpl.query(queryString);
    return rows[0];
  }

  async create(entry) {
    const { adminId } = this.store.ctx();
    const id = uuidv4();
    const entity = {
      employeeId: id,
      restaurantId: entry.restaurantId,
      adminId,
      employeeName: entry.employeeName,
      employeeSurname: entry.employeeSurname,
      employeeBirthday: entry.employeeBirthday,
      employeePhone: entry.employeePhone,
      employeeEmail: entry.employeeEmail,
      employeeType: entry.employeeType,
      contractType: entry.contractType,
      address: entry.address,
      startedWork: entry.startedWork,
      endedWork: entry.endedWork,
      employeeSalary: entry.employeeSalary,
    };

    const queryString = 'CALL sp_insert_employee(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });

    return { id };
  }

  async update(employeeId, entry) {
    const entity = {
      employeeId,
      employeePhone: entry.employeePhone,
      employeeEmail: entry.employeeEmail,
      employeeType: entry.employeeType,
      contractType: entry.contractType,
      address: entry.address,
      endedWork: entry.endedWork,
      employeeSalary: entry.employeeSalary,
    };

    const queryString = 'CALL sp_update_employee(?,?,?,?,?,?,?,?)';
    await this._mysqlImpl.query(queryString, { values: Object.values(entity) });
  }

  async delete(id) {
    const queryString = 'CALL sp_delete_employee(?)';
    await this._mysqlImpl.query(queryString, { values: [id] });
  }
}
