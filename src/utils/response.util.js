export function ok(res, data) {
  res.status(200).send({ status: 200, data });
}

export function createOk(res, data) {
  res.status(201).send({ status: 201, data });
}
