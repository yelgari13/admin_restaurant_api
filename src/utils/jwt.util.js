import { sign, verify } from 'jsonwebtoken';
import AppError from './appError.util';
import config from '../container/config';

export function generateToken(encode) {
  return sign(encode, config.JWT_SECRET, { expiresIn: '30d' });
}

export function jwtVerify(token) {
  let payload = null;

  verify(token, config.JWT_SECRET, function (error, decodedToken) {
    if (error) {
      throw new AppError(401, 'THE_TOKEN_IS_INVALID', 'The token provider is invalid');
    }

    payload = decodedToken;
  });

  return payload;
}
