import { ok } from '../utils/response.util';

export default class AuthController {
  constructor({ authService }) {
    this._authService = authService;

    this.login = this.login.bind(this);
  }

  async login(req, res, next) {
    try {
      const { username, password } = req.params;
      ok(res, await this._authService.login(username, password));
    } catch (error) {
      next(error);
    }
  }
}
