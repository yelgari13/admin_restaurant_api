import { createOk, ok } from '../utils/response.util';
import { createValidateInput } from './validators/adminInput.validator';

export default class AdminController {
  constructor({ adminService }) {
    this._adminService = adminService;

    this.getByUsername = this.getByUsername.bind(this);
    this.create = this.create.bind(this);
  }

  async getByUsername(req, res, next) {
    try {
      ok(res, await this._adminService.getByUsername(req.params.username));
    } catch (error) {
      next(error);
    }
  }

  async create(req, res) {
    try {
      createValidateInput(req.body);
      createOk(res, await this._adminService.create(req.body));
    } catch (error) {
      next(error);
    }
  }
}
