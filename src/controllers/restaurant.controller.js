import { createOk, ok } from '../utils/response.util';
import { createValidateInput } from './validators/restaurantInput.validator';

export default class RestaurantController {
  constructor({ restaurantService }) {
    this._restaurantService = restaurantService;

    this.getByAdmin = this.getByAdmin.bind(this);
    this.create = this.create.bind(this);
  }

  async getByAdmin(req, res, next) {
    try {
      ok(res, await this._restaurantService.getByAdmin());
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    try {
      createValidateInput(req.body);
      createOk(res, await this._restaurantService.create(req.body));
    } catch (error) {
      next(error);
    }
  }
}
