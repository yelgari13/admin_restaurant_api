import { createOk, ok } from '../utils/response.util';
import { createValidatorInput, updateValidateInput } from './validators/providerInput.validator';

export default class ProviderController {
  constructor({ providerService }) {
    this._providerService = providerService;

    this.search = this.search.bind(this);
    this.list = this.list.bind(this);
    this.getById = this.getById.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  async search(req, res, next) {
    try {
      const { restaurantId } = req.params;
      ok(res, await this._providerService.search(restaurantId, req.query));
    } catch (error) {
      next(error);
    }
  }

  async list(res, next) {
    try {
      ok(res, await this._providerService.list());
    } catch (error) {
      next(error);
    }
  }

  async getById(req, res, next) {
    try {
      ok(res, await this._providerService.getById(req.params.id));
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    try {
      createValidatorInput(req.body); // Validar los datos de entrada
      createOk(res, await this._providerService.create(req.body));
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const { id } = req.params;
      updateValidateInput(req.body);
      createOk(res, await this._providerService.update(id, req.body));
    } catch (error) {
      next(error);
    }
  }

  async delete(req, res, next) {
    try {
      const { id } = req.params;
      await this._providerService.delete(id);
      res.status(204).end();
    } catch (error) {
      next(error);
    }
  }
}