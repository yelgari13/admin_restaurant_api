import { createOk, ok } from '../utils/response.util';
import { createValidatorInput, updateValidateInput } from './validators/employeeInput.validator';

export default class EmployeeController {
  constructor({ employeeService }) {
    this._employeeService = employeeService;

    this.search = this.search.bind(this);
    this.list = this.list.bind(this);
    this.getById = this.getById.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  async search(req, res, next) {
    try {
      const { restaurantId } = req.params;
      ok(res, await this._employeeService.search(restaurantId, req.query));
    } catch (error) {
      next(error);
    }
  }

  async list(req, res, next) {
    try {
      ok(res, await this._employeeService.list());
    } catch (error) {
      next(error);
    }
  }

  async getById(req, res, next) {
    try {
      ok(res, await this._employeeService.getById(req.params.id));
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    try {
      createValidatorInput(req.body); // Validar los datos de entrada
      createOk(res, await this._employeeService.create(req.body));
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const { id } = req.params;
      updateValidateInput(req.body);
      createOk(res, await this._employeeService.update(id, req.body));
    } catch (error) {
      next(error);
    }
  }

  async delete(req, res, next) {
    try {
      const { id } = req.params;
      await this._employeeService.delete(id);
      res.status(204).end();
    } catch (error) {
      next(error);
    }
  }
}
