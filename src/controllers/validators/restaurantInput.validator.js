import Joi from 'joi';
import regex from '../../common/regex';
import AppError from '../../utils/appError.util';

export function createValidateInput(input) {
  const schema = Joi.object({
    adminId: Joi.string().required(),
    ruc: Joi.string().pattern(regex.regexRuc).required(),
    name: Joi.string().trim().required(),
    logoUrl: Joi.string().pattern(regex.regexURL).allow(null).required(),
    currencyId: Joi.string().required(),
    country: Joi.string().required(),
    departament: Joi.string().required(),
    province: Joi.string().required(),
    district: Joi.string().required(),
    address: Joi.string().required(),
    addressRefence: Joi.string().allow(null),
  });

  const { error } = schema.validate(input);

  if (error) {
    throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
  }
}
