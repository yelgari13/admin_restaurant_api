import Joi from 'joi';
import AppError from '../../utils/appError.util';

export function createValidatorInput(input) {
  const schema = Joi.object({
    restaurantId: Joi.string().required(),
    productName: Joi.string().required(),
    productType: Joi.string().required(),
    productBrand: Joi.string().required(),
    productDescription: Joi.string().required(),
    productStock: Joi.number().integer().min(0).required(),
    productPrice: Joi.number().precision(2).min(0).required(),
    productDiscount: Joi.number().precision(2).min(0).optional(),
    currencySymbol: Joi.string().required(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}

export function updateValidateInput(input) {
  const schema = Joi.object({
    productType: Joi.string().required(),
    productBrand: Joi.string().required(),
    productDescription: Joi.string().required(),
    productStock: Joi.number().integer().min(0).required(),
    productPrice: Joi.number().precision(2).min(0).required(),
    productDiscount: Joi.number().precision(2).min(0).optional(),
    currencySymbol: Joi.string().required(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}
