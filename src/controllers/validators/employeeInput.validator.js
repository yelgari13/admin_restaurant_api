import Joi from 'joi';
import AppError from '../../utils/appError.util';

export function createValidatorInput(input) {
  const schema = Joi.object({
    restaurantId: Joi.string().required(),
    employeeName: Joi.string().required(),
    employeeSurname: Joi.string().required(),
    employeeBirthday: Joi.date().required(),
    employeePhone: Joi.string().required(),
    employeeEmail: Joi.string().required(),
    employeeType: Joi.string().required(),
    contractType: Joi.string().required(),
    address: Joi.string().required(),
    startedWork: Joi.date().required(),
    endedWork: Joi.date(),
    employeeSalary: Joi.number().required(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}

export function updateValidateInput(input) {
  const schema = Joi.object({
    employeeBirthday: Joi.date().required(),
    employeePhone: Joi.string().required(),
    employeeEmail: Joi.string().required(),
    employeeType: Joi.string().required(),
    contractType: Joi.string().required(),
    address: Joi.string().required(),
    startedWork: Joi.string().required(),
    endedWork: Joi.string(),
    employeeSalary: Joi.number().required(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}
