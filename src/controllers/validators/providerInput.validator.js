import Joi from 'joi';
import AppError from '../../utils/appError.util';

export function createValidatorInput(input) {
  const schema = Joi.object({
    restaurantId: Joi.string().required(),
    providerBussinessName: Joi.string().required(),
    providerRuc: Joi.string().required(),
    providerPhone: Joi.string().required(),
    providerEmail: Joi.string().required(),
    country: Joi.string().required(),
    departament: Joi.string().required(),
    province: Joi.string().required(),
    district: Joi.string().required(),
    address: Joi.string().required(),
    addressReference: Joi.string(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}

export function updateValidateInput(input) {
  const schema = Joi.object({
    providerPhone: Joi.string().required(),
    providerEmail: Joi.string().required(),
    country: Joi.string().required(),
    departament: Joi.string().required(),
    province: Joi.string().required(),
    district: Joi.string().required(),
    address: Joi.string().required(),
    addressReference: Joi.string(),
  });

  const { error } = schema.validate(input);
  if (error) throw new AppError(400, 'ERROR_REQUEST_VALIDATE_INPUT', error.details[0].message);
}
