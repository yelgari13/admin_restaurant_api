import mysql from 'mysql2/promise';
import clc from 'cli-color';

export default class CloudConnectionMySQL {
  constructor({ config }) {
    this._config = config;
    this.pool = null;
  }

  async connect() {
    const connection = await mysql.createConnection({
      host: this._config.DB_MYSQL_HOST,
      user: this._config.DB_MYSQL_USER,
      password: this._config.DB_MYSQL_PASSWORD,
      database: this._config.DB_MYSQL_NAME,
    });

    await connection.connect();
    console.log(clc.bgBlue(`The database mysql is connected ${this._config.DB_MYSQL_NAME}`));

    this.pool = connection;
  }

  /**
   * @param {string} sqlString
   * @param {Object} [queryOptions]
   * @param {string} queryOptions.values
   *
   */
  async query(queryString, queryOptions) {
    return this.pool.query({ sql: queryString, ...queryOptions });
  }
}
