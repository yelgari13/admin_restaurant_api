import { Router } from 'express';

export default function ({ adminController }) {
  const router = Router();

  router.get('/:username', adminController.getByUsername);
  router.post('/create', adminController.create);

  return router;
}
