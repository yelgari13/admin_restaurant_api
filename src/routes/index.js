import express from 'express';
import 'express-async-error';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import compression from 'compression';
import errorMiddleware from '../middlewares/errorException.middleware';
import notFoundMiddleware from '../middlewares/notFoundException.middleware';

export default function ({ homeRoutes, authRoutes, adminRoutes, restaurantRoutes, providerRoutes, productRoutes ,employeeRoutes}) {
  const router = express.Router();
  const apiRoutes = express.Router();

  // Middleware default
  apiRoutes
    .use(express.json())
    .use(express.urlencoded({ extended: false }))
    .use(cors())
    .use(helmet())
    .use(compression())
    .use(morgan('dev'));

  // Prefijo de ruta
  router.use('/v1', apiRoutes);

  // Middleware setting
  router.use(notFoundMiddleware);
  router.use(errorMiddleware);

  // Funciones
  apiRoutes.use('/', homeRoutes);
  apiRoutes.use('/auth', authRoutes);
  apiRoutes.use('/admins', adminRoutes); //routes deprecate why sensitive data
  apiRoutes.use('/restaurants', restaurantRoutes);
  apiRoutes.use('/providers', providerRoutes);
  apiRoutes.use('/products', productRoutes); // Agregar las rutas de productos
  apiRoutes.use('/employees', employeeRoutes); // Agregar las rutas de employees
  

  return router;
}
