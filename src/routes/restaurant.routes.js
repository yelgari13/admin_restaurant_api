import { Router } from 'express';
import { authMiddleware } from '../middlewares/auth.middleware';

export default function ({ restaurantController }) {
  const router = Router();

  router.get('/admin', [authMiddleware], restaurantController.getByAdmin);
  router.post('/create', restaurantController.create);

  return router;
}
