import { Router } from 'express';
import { authMiddleware } from '../middlewares/auth.middleware';
import parseInt from '../middlewares/parseInt.middleware';

export default function ({ productController }) {
  const router = Router();

  router.get('/:restaurantId/search', [authMiddleware, parseInt], productController.search);
  router.get('/list', productController.list);
  router.get('/:id', [authMiddleware], productController.getById);
  router.post('/create', [authMiddleware], productController.create);
  router.put('/update/:id', [authMiddleware], productController.update);
  router.delete('/delete/:id', [authMiddleware], productController.delete);

  return router;
}
