import { Router } from 'express';
import { authMiddleware } from '../middlewares/auth.middleware';
import parseInt from '../middlewares/parseInt.middleware';

export default function ({ employeeController }) {
  const router = Router();

  router.get('/:restaurantId/search', [authMiddleware, parseInt], employeeController.search);
  router.get('/list', employeeController.list);
  router.get('/:id', [authMiddleware], employeeController.getById);
  router.post('/create', [authMiddleware], employeeController.create);
  router.put('/update/:id', [authMiddleware], employeeController.update);
  router.delete('/delete/:id', [authMiddleware], employeeController.delete);

  return router;
}
