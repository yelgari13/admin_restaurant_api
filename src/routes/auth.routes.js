import { Router } from 'express';

export default function ({ authController }) {
  const router = Router();

  router.post('/login/:username/:password', authController.login);

  return router;
}
