import { Router } from 'express';
import { authMiddleware } from '../middlewares/auth.middleware';
import parseInt from '../middlewares/parseInt.middleware';

export default function ({ providerController }) {
  const router = Router();

  router.get('/:restaurantId/search', [authMiddleware, parseInt], providerController.search);
  router.get('/list', providerController.list);
  router.get('/:id', [authMiddleware], providerController.getById);
  router.post('/create', [authMiddleware], providerController.create);
  router.put('/update/:id', [authMiddleware], providerController.update);
  router.delete('/delete/:id', [authMiddleware], providerController.delete);

  return router;
}


