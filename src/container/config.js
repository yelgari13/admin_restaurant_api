if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

export default {
  PORT: process.env.PORT || 4000,
  DB_MYSQL_HOST: process.env.DB_MYSQL_HOST,
  DB_MYSQL_USER: process.env.DB_MYSQL_USER,
  DB_MYSQL_PASSWORD: process.env.DB_MYSQL_PASSWORD,
  DB_MYSQL_NAME: process.env.DB_MYSQL_NAME,
  JWT_SECRET: process.env.JWT_SECRET,
};
