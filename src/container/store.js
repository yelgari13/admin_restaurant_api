import { containerMiddleware } from '../middlewares/auth.middleware';

export default class Store {
  ctx() {
    return {
      adminId: containerMiddleware.resolve('adminId'),
    };
  }
}
