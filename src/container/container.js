import { createContainer, asClass, asFunction, asValue, Lifetime } from 'awilix';

// setting container app
import pkg from '../../package.json';
import config from './config';
import server from './server';
import routes from '../routes/index';
import Store from '../container/store';

const container = createContainer();
container.register({
  pkg: asValue(pkg),
  config: asValue(config),
  server: asClass(server).singleton(),
  routes: asFunction(routes).singleton(),
  store: asClass(Store).singleton(),
});

container.loadModules(['databases/**/*.db.js', 'services/**/*.service.js', 'controllers/**/*.controller.js', 'routes/**/*.routes.js'], {
  cwd: `${__dirname}/..`,
  formatName: 'camelCase',
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
  },
});

export default container;
