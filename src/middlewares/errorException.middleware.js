export default function (err, req, res, next) {
  if (err.errorCode) {
    const statusHttp = err.errorStatus || 400;
    res.status(statusHttp);
    res.send({
      status: statusHttp,
      code: err.errorCode,
      message: err.message,
    });
  } else {
    res.status(500);
    res.send({
      status: 500,
      code: 'INTERNAL_SERVER_ERROR',
      message: err?.message || JSON?.stringify(err) || null,
    });
  }
}
