import { createContainer, asValue, InjectionMode } from 'awilix';
import AppError from '../utils/appError.util';
import { jwtVerify } from '../utils/jwt.util';

export const containerMiddleware = createContainer({ injectionMode: InjectionMode.CLASSIC });

export function authMiddleware(req, res, next) {
  const token = req.headers.authorization && req.headers.authorization.replace('Bearer ', '');

  if (!token) {
    throw new AppError(400, 'THE_TOKEN_NO_PROVIDER', 'A token is required');
  }

  const payload = jwtVerify(token);

  containerMiddleware.register({
    adminId: asValue(payload.adminId),
  });

  next();
}
