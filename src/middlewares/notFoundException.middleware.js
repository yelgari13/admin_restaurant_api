export default function (req, res, nex) {
  res.status(404).send({
    status: 404,
    code: 'RESOURCE_NOT_FOUND',
  });
}
