-- Procedimiento almacenado para eliminar un proveedor
DELIMITER //
CREATE PROCEDURE sp_delete_product(
    IN p_productId VARCHAR(255)
)
BEGIN
	DELETE FROM provider WHERE productId = p_productId;
END //
DELIMITER ;