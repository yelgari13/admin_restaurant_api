DELIMITER $$
CREATE PROCEDURE sp_delete_employee(
    IN p_employeeId VARCHAR(255)
)
BEGIN
    DELETE FROM employee WHERE employeeId = p_employeeId;
END$$
DELIMITER ;
