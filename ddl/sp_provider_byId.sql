-- Procedimiento almacenado para buscar un proveedor por su ID
DELIMITER //
CREATE PROCEDURE sp_provider_byId(
    IN p_providerId VARCHAR(255)
)
BEGIN
    SELECT * FROM provider WHERE providerId = p_providerId;
END //

DELIMITER ;