DELIMITER $$
CREATE PROCEDURE sp_employee_by_id(
    IN p_employeeId VARCHAR(255)
)
BEGIN
    SELECT *
    FROM employee
    WHERE employeeId = p_employeeId;
END$$
DELIMITER ;
