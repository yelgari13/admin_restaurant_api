DELIMITER //
create procedure sp_admin_by_username(in username varchar(255))
begin
	select * from admin where adminUsername = username;
end //
DELIMITER ;