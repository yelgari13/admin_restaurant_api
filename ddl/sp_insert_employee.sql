DELIMITER $$
CREATE PROCEDURE sp_insert_employee(
    IN p_employeeId VARCHAR(255),
    IN p_restaurantId VARCHAR(255),
    IN p_adminId VARCHAR(255),
    IN p_employeeName VARCHAR(255),
    IN p_employeeSurname VARCHAR(255),
    IN p_employeeBirthday DATE,
    IN p_employeePhone VARCHAR(255),
    IN p_employeeEmail VARCHAR(255),
    IN p_employeeType VARCHAR(255),
    IN p_contractType VARCHAR(255),
    IN p_address VARCHAR(255),
    IN p_startedWork DATE,
    IN p_endedWork DATE,
    IN p_employeeSalary VARCHAR(255)
)
BEGIN
    INSERT INTO employee (
        employeeId, 
        restaurantId, 
        adminId, 
        employeeName, 
        employeeSurname, 
        employeeBirthday, 
        employeePhone, 
        employeeEmail, 
        employeeType, 
        contractType, 
        address, 
        startedWork, 
        endedWork, 
        employeeSalary
    ) 
    VALUES (
        p_employeeId, 
        p_restaurantId, 
        p_adminId, 
        p_employeeName, 
        p_employeeSurname, 
        p_employeeBirthday, 
        p_employeePhone, 
        p_employeeEmail, 
        p_employeeType, 
        p_contractType, 
        p_address, 
        p_startedWork, 
        p_endedWork, 
        p_employeeSalary
    );
END$$
DELIMITER ;
