-- Procedimiento almacenado para insertar un nuevo proveedor
DELIMITER //

CREATE PROCEDURE sp_insert_provider(
    IN p_providerId VARCHAR(255),
    IN p_restaurantId VARCHAR(255),
    IN p_adminId VARCHAR(255),
    IN p_providerBussinessName VARCHAR(255),
    IN p_providerRuc VARCHAR(255),
    IN p_providerPhone VARCHAR(255),
    IN p_providerEmail VARCHAR(255),
    IN p_country VARCHAR(255),
    IN p_departament VARCHAR(255),
    IN p_province VARCHAR(255),
    IN p_district VARCHAR(255),
    IN p_address VARCHAR(255),
    IN p_addressReference VARCHAR(255),
    IN p_IsDelete TINYINT(1)
)
BEGIN
    INSERT INTO provider (providerId, restaurantId, adminId, providerBussinessName, providerRuc, providerPhone, providerEmail, country, departament, province, district, address, addressReference, IsDelete)
    VALUES (p_providerId, p_restaurantId, p_adminId, p_providerBussinessName, p_providerRuc, p_providerPhone, p_providerEmail, p_country, p_departament, p_province, p_district, p_address, p_addressReference, p_IsDelete);
END //
DELIMITER ;