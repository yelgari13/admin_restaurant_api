-- Procedimiento almacenado para eliminar un proveedor
DELIMITER //
CREATE PROCEDURE sp_delete_provider(
    IN p_providerId VARCHAR(255)
)
BEGIN
	DELETE FROM provider WHERE providerId = p_providerId;
END //
DELIMITER ;