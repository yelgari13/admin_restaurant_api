CREATE DEFINER=`u8slnbjhnghqhusy`@`%` PROCEDURE `sp_product_byId`(
    IN p_productId VARCHAR(255)
)
BEGIN
    SELECT *
    FROM product
    WHERE productId = p_productId;
END
