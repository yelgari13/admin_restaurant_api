-- Procedimiento almacenado para listar todos los proveedores
DELIMITER //
CREATE PROCEDURE sp_list_provider()
BEGIN
    SELECT * FROM provider WHERE IsDelete = 0;
END //
DELIMITER ;