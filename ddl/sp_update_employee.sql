DELIMITER $$
CREATE PROCEDURE sp_update_employee(
    IN p_employeeId VARCHAR(255),
    IN p_employeePhone VARCHAR(255),
    IN p_employeeEmail VARCHAR(255),
    IN p_employeeType VARCHAR(255),
    IN p_contractType VARCHAR(255),
    IN p_address VARCHAR(255),
    IN p_endedWork DATE,
    IN p_employeeSalary VARCHAR(255)
)
BEGIN
    UPDATE employee
    SET 
        employeePhone = p_employeePhone,
        employeeEmail = p_employeeEmail,
        employeeType = p_employeeType,
        contractType = p_contractType,
        address = p_address,
        endedWork = p_endedWork,
        employeeSalary = p_employeeSalary
    WHERE employeeId = p_employeeId;
END$$
DELIMITER ;
