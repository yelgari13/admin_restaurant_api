DELIMITER //
create procedure sp_restaurant_by_admin(in adminId varchar(255))
begin
	select * from restaurant where adminId = adminId;
end //
DELIMITER ;