DELIMITER //
create procedure sp_insert_admin(
	in adminId varchar(255),
    adminName varchar(255),
    adminUsername varchar(255),
    adminPassword varchar(255)
)
begin
	insert into admin(adminId, adminName, adminUsername, adminPassword) values
    (adminId, adminName, adminUsername, adminPassword);
end //
DELIMITER ;