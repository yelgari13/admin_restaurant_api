CREATE DEFINER=`u8slnbjhnghqhusy`@`%` PROCEDURE `sp_update_product`(
    IN p_productId VARCHAR(255),
    IN p_productName VARCHAR(255),
    IN p_productType VARCHAR(255),
    IN p_productBrand VARCHAR(255),
    IN p_productDescription VARCHAR(255),
    IN p_productStock INT,
    IN p_productPrice DECIMAL(10,2),
    IN p_productDiscount DECIMAL(10,2),
    IN p_currencySymbol VARCHAR(100))
BEGIN
    UPDATE product
    SET 
        productName = p_productName,
        productType = p_productType,
        productBrand = p_productBrand,
        productDescription = p_productDescription,
        productStock = p_productStock,
        productPrice = p_productPrice,
        productDiscount = p_productDiscount,
        currencySymbol = p_currencySymbol
    WHERE productId = p_productId;
END
