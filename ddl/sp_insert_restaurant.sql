DELIMITER //
create procedure sp_insert_restaurant(
	in restaurantId varchar(255),
    adminId varchar(255),
    restaurantRuc varchar(255),
    restaurantName varchar(255),
    restaurantLogoUrl longtext,
    currencyId varchar(255),
    country varchar(255),
    departament varchar(255),
    province varchar(255),
    district varchar(255),
	address varchar(255),
	addressRefence varchar(255)
)
begin
	insert into restaurant(
		restaurantId, 
    adminId, 
    restaurantRuc, 
    restaurantName, 
    restaurantLogoUrl, 
    currencyId,
    country,
    departament,
    province,
    district,
    address,
    addressRefence
	) values (
		restaurantId, 
    adminId, 
    restaurantRuc, 
    restaurantName, 
    restaurantLogoUrl, 
    currencyId,
    country,
    departament,
    province,
    district,
    address,
    addressRefence
	);
end //
DELIMITER ;