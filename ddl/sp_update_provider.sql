-- Procedimiento almacenado para actualizar un proveedor
DELIMITER //
CREATE PROCEDURE sp_update_provider(
    IN p_providerId VARCHAR(255),
    IN p_providerPhone VARCHAR(255),
    IN p_providerEmail VARCHAR(255),
    IN p_country VARCHAR(255),
    IN p_departament VARCHAR(255),
    IN p_province VARCHAR(255),
    IN p_district VARCHAR(255),
    IN p_address VARCHAR(255),
    IN p_addressReference VARCHAR(255)
)
BEGIN
    UPDATE provider
    SET
        providerPhone = p_providerPhone,
        providerEmail = p_providerEmail,
        country = p_country,
        departament = p_departament,
        province = p_province,
        district = p_district,
        address = p_address,
        addressReference = p_addressReference
    WHERE
        providerId = p_providerId;
END //
DELIMITER ;
