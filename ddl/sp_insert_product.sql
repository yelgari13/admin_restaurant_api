DELIMITER //
CREATE PROCEDURE sp_insert_product(
    IN productId VARCHAR(255),
    IN restaurantId VARCHAR(255),
    IN adminId VARCHAR(255),
    IN productName VARCHAR(255),
    IN productType VARCHAR(200),
    IN productBrand VARCHAR(200),
    IN productDescription VARCHAR(255),
    IN productStock INT,
    IN productPrice DECIMAL(10,2),
    IN productDiscount DECIMAL(10,2),
    IN currencySymbol VARCHAR(100),
    IN p_IsDelete TINYINT(1)
)
BEGIN
    INSERT INTO product(
        productId,
        restaurantId,
        adminId,
        productName,
        productType,
        productBrand,
        productDescription,
        productStock,
        productPrice,
        productDiscount,
        currencySymbol,
        IsDelete
    ) VALUES (
        productId,
        restaurantId,
        adminId,
        productName,
        productType,
        productBrand,
        productDescription,
        productStock,
        productPrice,
        productDiscount,
        currencySymbol,
        IsDelete
    );
END //
DELIMITER ;
